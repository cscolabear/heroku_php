<?php

use Parse\ParseQuery;

class ParseHelper{

	private static $_instance=array();

    public static function gen($className=__CLASS__)
    {
        if(isset(self::$_instance[$className])) {
            return self::$_instance[$className];
        } else {
            $model=self::$_instance[$className]=new $className(null);
            return $model;
        }
    }

	function getActionList($_limit = null, $_skip = null)
	{
		$data = array();

		$query = new ParseQuery("Idea");

        $query->includeKey('categoryPointer');
        $query->includeKey('graphicPointer');

        if($_limit) $query->limit($_limit);
        if($_skip) $query->limit($_skip);

        $query->equalTo("language", "zh");
        $query->notEqualTo("status", "close");
        $query->descending("updatedAt");
        $query->descending("createdAt");
        $results = $query->find();

        if($results)
        foreach ($results as $key => $object) {
            $data[$key] = array(
                    'objectId'         => $object->getObjectId(),
                    'Category'         => $object->get('categoryPointer')->get('Name'),
                    'Name'             => $object->get('Name'), 
                    'Desc'             => $object->get('Description'),
                    'webUrl'           => $object->get('webUrl'), 
                    'webUrlActionCall' => $object->get('webUrlActionCall'), 
                    'doneCount'        => $object->get('doneCount'), 
                    // 'imageUrl'         => $object->get('graphicPointer')->get('imageFile')->getURL(),
                );

            if($object->get('graphicPointer')) {
                $data[$key]['imageUrl'] = $object->get('graphicPointer')->get('imageFile')->getURL();
            }
		}//^ foreach

		unset($results);
		return $data;
	}

	function getAction($oid)
	{
		$data = array();

		$query = new ParseQuery("Idea");

        $query->includeKey('categoryPointer');
        $query->includeKey('graphicPointer');

		$query->limit(1);

        $query->equalTo("objectId", $oid);
        $results = $query->find();

        if($object = $results[0]) {
            $data = array(
                    'objectId'         => $object->getObjectId(),
                    'Category'         => $object->get('categoryPointer')->get('Name'),
                    'Name'             => $object->get('Name'), 
                    'Desc'             => $object->get('Description'),
                    'webUrl'           => $object->get('webUrl'), 
                    'webUrlActionCall' => $object->get('webUrlActionCall'), 
                    'doneCount'        => $object->get('doneCount'), 
                    // 'imageUrl'         => $object->get('graphicPointer')->get('imageFile')->getURL(),
                );
            if($object->get('graphicPointer')) {
                $data['imageUrl'] = $object->get('graphicPointer')->get('imageFile')->getURL();
            }
		}//^ if

		unset($results);
		return $data;
	}

	function getStory($ideaId)
	{
		$data = array();

		$query = new ParseQuery("Story");

		$query->includeKey('StoryTeller');


        $query->equalTo("ideaPointer", array(
            "__type" => "Pointer",
            "className" => "Idea", 
            "objectId" => $ideaId
            ));
        
        $query->descending("updatedAt");
        $query->descending("createdAt");
        $results = $query->find();

        if($results)
        foreach ($results as $key => $object) {
            $data[] = array(
					'objectId' => $object->getObjectId(),
					'Content'  => $object->get('Content'), 
					'author'   => $object->get('StoryTeller')->get('name'),
                );
		}//^ foreach

		unset($results);
		return $data;
	}
}
