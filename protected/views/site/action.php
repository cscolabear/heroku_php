<div class="center-block">
<?php
if(is_array($DATA)) {
    $obj = $DATA;
?>
        <div class="action_block col-xs-12">
            <div class="action_img pull-left col-sm-4 col-xs-12">
                <img src="<?php echo $obj['imageUrl'];?>" alt="<?php echo $obj['Name'];?>" class="img-thumbnail">
            </div>
            <h3><p class="bg-danger"><?php echo $obj['Category'];?></p></h3>
            <?php echo $obj['Name'];?>
            
            <div class="panel panel-default">
                <div class="panel-body">
                    <?php echo nl2br($obj['Desc']);?>
                </div>
            </div>
            <br />

            <?php if($obj['webUrl'] && $obj['webUrlActionCall']):?>
                <p class="text-danger">
                    <a href="<?php echo $obj['webUrl'];?>" target="_blank"><?php echo $obj['webUrlActionCall'];?></a>
                </p>
            <?php endif;?>

            <?php if($obj['doneCount'] > 0):?>
                <a href="story/<?php echo $obj['objectId'];?>">
                    <span class="pull-right label label-info">已有 <?php echo $obj['doneCount'];?> 個響應行動</span>
                </a>
            <?php endif;?>
        </div>
<?php
}//^ if
?>
</div>