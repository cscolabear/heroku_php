<div class="center-block">
<?php
if(is_array($DATA)) {
    foreach ($DATA as $key => $obj) {
    ?>
    <a name="<?php echo $obj['objectId'];?>" id="<?php echo $obj['objectId'];?>" href="action/<?php echo $obj['objectId'];?>">
        <div class="myblock col-md-3 col-sm-6 col-xs-12">
            <div class="list_img pull-left">
                <img src="<?php echo $obj['imageUrl'];?>" alt="<?php echo $obj['Name'];?>" class="img-thumbnail">
            </div>
            <h3><p class="bg-danger"><?php echo $obj['Category'];?></p></h3>
            <?php echo $obj['Name'];?>
            <br />
            <br />
            <br />

            <!--<?php echo $obj['Desc'];?><br />-->
            <?php if($obj['doneCount'] > 0):?>
                <span class="pull-right label label-info">已有 <?php echo $obj['doneCount'];?> 個響應行動</span>
            <?php endif;?>


            <!--<?php echo $obj['webUrl'];?><br />-->
            <!--<?php echo $obj['webUrlActionCall'];?><br />-->
        </div>
    </a>
<?php
    }//^ foreach
}//^ if
?>
</div>