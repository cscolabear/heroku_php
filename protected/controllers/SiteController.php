<?php

class SiteController extends Controller
{
	/**
	 * This is the default 'index' action that is invoked
	 * when an action is not explicitly requested by users.
	 */
	public function actionIndex()
	{
        $data = ParseHelper::gen()->getActionList();
		$this->render('index', array('DATA' => $data));
	}

    public function actionContent($oid)
    {
        if($oid) {
            $data = ParseHelper::gen()->getAction($oid);
        }
        $this->render('action', array('DATA' => $data));   
    }

    public function actionStory($oid)
    {
        if($oid) {
            $data = ParseHelper::gen()->getStory($oid);
        }
        $this->render('story', array('DATA' => $data));   
    }


	/**
	 * This is the action to handle external exceptions.
	 */
	public function actionError()
	{
		if($error=Yii::app()->errorHandler->error)
		{
			if(Yii::app()->request->isAjaxRequest)
				echo $error['message'];
			else
				$this->render('error', $error);
		}
	}

}